package assignment.restcontroller;

import assignment.dao.UserDao;
import assignment.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gareth1305 on 20/11/2015.
 */
@RestController
public class UserRestController {

    UserDao userDao;

    @Autowired
    public UserRestController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping(value="/restusers")
    public List<User> listUsers() {
        List<User> users = (List<User>) userDao.findAll();
        return users;
    }

    @RequestMapping(value = "create_user", method = RequestMethod.POST)
    @ResponseBody
    public String savePerson(User user) {
        userDao.save(user);
        return "User Saved: " + user.toString();
    }

}
