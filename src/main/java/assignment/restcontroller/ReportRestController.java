package assignment.restcontroller;

import assignment.dao.ReportDao;
import assignment.dao.UserDao;
import assignment.entity.Report;
import assignment.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gareth1305 on 20/11/2015.
 */
@RestController
public class ReportRestController {

    ReportDao reportDao;

    @Autowired
    public ReportRestController(ReportDao reportDao) {
        this.reportDao = reportDao;
    }

    @RequestMapping(value="/restreports")
    public List<Report> listReports() {
        List<Report> reports = (List<Report>) reportDao.findAll();
        return reports;
    }

    @RequestMapping(value = "create_report", method = RequestMethod.POST)
    @ResponseBody
    public String saveReport(Report report) {
        reportDao.save(report);
        return "Report Saved: " + report.toString();
    }

}
