package assignment.controller;

import assignment.dao.ReportDao;
import assignment.dao.UserDao;
import assignment.entity.Report;
import assignment.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Gareth1305 on 20/11/2015.
 */
@Controller
public class MainController {

    ReportDao reportDao;
    UserDao userDao;

    @RequestMapping("/")
    @ResponseBody
    public String index() throws ParseException {
        return "This service has been developed by Gareth1305@gmail.com";
    }

    @Autowired
    public MainController(ReportDao reportDao, UserDao userDao) {
        this.reportDao = reportDao;
        this.userDao = userDao;
    }

}