package assignment.dao;

import assignment.entity.Report;
import assignment.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface ReportDao extends CrudRepository<Report, Long> {

    public Iterable<Report> findAll();

}